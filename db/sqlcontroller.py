from pymysql import *


class SQLController:
    def __init__(self, host="", port=0, db="", user="", password=""):
        self.host = host
        self.port = port
        self.db = db
        self.user = user
        self.password = password

    def select_all(self, table_name=""):
        """ Method for retrieving all values
            Input parameters are table_name (string) """
        conn = connect(host=self.host, port=self.port, db=self.db, user=self.user, password=self.password)
        cursor = conn.cursor()
        sql = "SELECT * FROM {}".format(table_name)
        cursor.execute(sql)
        result = cursor.fetchall()
        conn.close()
        return result

    def select_one(self, table_name=""):
        """ Method for retrieving all values
            Input parameters are table_name (string) """
        conn = connect(host=self.host, port=self.port, db=self.db, user=self.user, password=self.password)
        cursor = conn.cursor()
        sql = "SELECT * FROM {}".format(table_name)
        cursor.execute(sql)
        result = cursor.fetchone()
        conn.close()
        return result

    def update(self, table_name="", column_name="", new_value="", column_identifier="", identifier_value=""):
        """ Method for updating a table
            Input parameters are table_name (string) """
        conn = connect(host=self.host, port=self.port, db=self.db, user=self.user, password=self.password)
        cursor = conn.cursor()
        conn.begin()  # begin transaction

        if isinstance(new_value, str):
            new_value = "'" + new_value + "'"

        if isinstance(identifier_value, str):
            identifier_value = "'" + identifier_value + "'"

        sql = "UPDATE {} SET {}={} WHERE {}={}".format(table_name, column_name, new_value, column_identifier, identifier_value)

        cursor.execute(sql)
        conn.commit()
        conn.close()
        print("Query executed successfully")
        return 0

    def insert(self, table_name="", keys="", new_value=""):
        """ Method for updating a table
                    Input parameters are table_name (string) """
        conn = connect(host=self.host, port=self.port, db=self.db, user=self.user, password=self.password)
        cursor = conn.cursor()
        conn.begin()  # begin transaction
        sql = "INSERT INTO {} {} VALUES {}".format(table_name, keys, str(new_value))
        cursor.execute(sql)
        conn.commit()
        conn.close()
        print("Query executed successfully")
        return 0

    def delete(self, table_name="", keys="", del_value=""):
        conn = connect(host=self.host, port=self.port, db=self.db, user=self.user, password=self.password)
        cursor = conn.cursor()
        conn.begin()  # begin transaction
        sql = "DELETE FROM {} WHERE {}={}".format(table_name, keys, str(del_value))
        cursor.execute(sql)
        conn.commit()
        conn.close()
        print("Query executed successfully")
        return 0