def blossoms(event, controller):
    user = event['user']
    bls = controller.select_all('citizens')
    jh_bls = bls[0][2]
    dy_bls = bls[1][2]

    if user == 'URWH8R9LZ' or 'UV4QCLGN5':  # 다연
        if event['reaction'] == 'cherry_blossom':
            controller.update('citizens', 'blossoms', jh_bls+1, 'user_id', bls[0][0])

    if user == 'UHW7BM13K' or 'U0100T671QD':  # 지훈
        if event['reaction'] == 'blossom':
            controller.update('citizens', 'blossoms', dy_bls+1, 'user_id', bls[1][0])