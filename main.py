import hashlib
import hmac
import json
import logging
import os
import sys
from logging import StreamHandler
from logging.handlers import RotatingFileHandler
from multiprocessing import Process
from time import time

from dotenv import load_dotenv
from flask import Flask, request, make_response
from slack_sdk import WebClient

from core import core

# Load Environment Variables into System
load_dotenv(verbose=True)

# Instantiate a new Flask app
app = Flask(__name__)

# Slack Endpoint
endpoint = '/slack/events'

# Slack keys from Env
signing_secret = os.environ['SLACK_SIGNING_SECRET']
slack_token = os.environ["SLACK_API_TOKEN"]

# Add path of geckodriver to PATH
os.environ['PATH'] += ':'+os.path.dirname(os.path.realpath(__file__))+"/webdriver"

# Slack Python Web Client
client = WebClient(token=slack_token)


# Verify Signature of request
def verify_signature(timestamp, signature):
    # Verify the request signature of the request sent from Slack
    # Generate a new hash using the app's signing secret and request data
    if hasattr(hmac, "compare_digest"):
        req = str.encode('v0:' + str(timestamp) + ':') + request.get_data()
        request_hash = 'v0=' + hmac.new(
            str.encode(signing_secret),
            req, hashlib.sha256
        ).hexdigest()
        return hmac.compare_digest(request_hash, signature)


# Flask app route bound to slack endpoint
@app.route(endpoint, methods=['GET', 'POST'])
def event():
    # Slack requests are POST
    if request.method == 'GET':
        return make_response("Requires a POST request", 404)

    # Requests comes with 'request timestamp' and 'request signature'
    req_timestamp = request.headers.get('X-Slack-Request-Timestamp')
    req_signature = request.headers.get('X-Slack-Signature')

    if abs(time() - int(req_timestamp)) > 60 * 5:
        return make_response("'Invalid request timestamp'", 403)

    if not verify_signature(req_timestamp, req_signature):
        return make_response("'Invalid request signature'", 403)

    # Parse request as json
    event_data = json.loads(request.data.decode('utf-8'))

    # Echo the URL verification challenge code back to Slack
    if "challenge" in event_data:
        return make_response(
            event_data.get("challenge"), 200, {"content_type": "application/json"}
        )

    # Parse the Event payload and emit the event to the event listener
    if "event" in event_data:
        event_type = event_data["event"]["type"]

        # Main function that acts on request
        p = Process(target=core, args=(event_type, event_data, client))
        p.start()

        response = make_response("", 200)
        return response


# Home lol
@app.route("/", methods=['GET', 'POST'])
def index():

    response = make_response("Good Good", 200)
    return response


if __name__ == "__main__":
    logfile_handler = RotatingFileHandler('bot.log', maxBytes=1000000, backupCount=1)
    stream_handler = StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    logfile_handler.setLevel(logging.DEBUG)
    logfile_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)
    app.logger.addHandler(logfile_handler)
    app.logger.addHandler(stream_handler)
    app.logger.setLevel(logging.DEBUG)
    app.run(host="0.0.0.0", port=80)

