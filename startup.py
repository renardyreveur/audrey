import requests
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
import time

# Get Public URL given by ngrok
ngrok_tunnel = requests.get('http://localhost:4040/api/tunnels')
public_url = ngrok_tunnel.json()['tunnels'][0]['public_url'] + "/slack/events"
print(public_url)

# Using Selenium to automate login and changing request URL
opts = Options()
# opts.headless = True
browser = Firefox(options=opts, service_log_path="/dev/null")
browser.get('https://api.slack.com')
time.sleep(1)

# Login button
browser.find_element_by_xpath('/html/body/header/div[1]/div[3]/div/a').click()

# Write down workspace and submit
workspace = browser.find_element_by_xpath('//*[@id="domain"]')
workspace.send_keys('aicomtrue')
browser.find_element_by_xpath('//*[@id="submit_team_domain"]').click()

# Login details sleep or else too frequent attempts
email = browser.find_element_by_xpath('//*[@id="email"]')
password = browser.find_element_by_xpath('//*[@id="password"]')
time.sleep(1)
email.send_keys('jhkang.comtrue@gmail.com')
time.sleep(1)
password.send_keys('wlgnsdudn')
time.sleep(1)

browser.find_element_by_xpath('//*[@id="signin_btn"]').click()

# Go back to api page
time.sleep(1)
browser.get('https://api.slack.com')

# To 'Your apps'
browser.find_element_by_xpath('/html/body/header/div[1]/div[3]/div/div/a[3]').click()

# dlcbot App
browser.find_element_by_xpath('/html/body/div[1]/div/div[1]/div[2]/table/tbody/tr[2]/td[1]/a').click()

# Event Subscription
browser.find_element_by_xpath('/html/body/div[1]/div/nav/div[1]/ul[2]/li[6]/a').click()

# Change URL Button
browser.find_element_by_xpath('//*[@id="change_request_url"]').click()
time.sleep(1)

# Type in new ngrok public url
new_url = browser.find_element_by_xpath('//*[@id="request_url"]')
new_url.send_keys(public_url)
time.sleep(10)
browser.find_element_by_xpath('//*[@id="page"]').click()

# If verified, save changes
verify = browser.find_element_by_xpath('/html/body/div[1]/div/div[1]/div[1]/div[2]/div[1]/label[2]/span/p').text
if 'Verified' in verify:
    time.sleep(1)
    browser.find_element_by_xpath('/html/body/div[3]/div/div/button[2]').click()
    print("Successful!")
else:
    print("Not Verified!!")

# Quit Browser
browser.quit()
