import os
import time

import schedule
from dotenv import load_dotenv
from slack import WebClient

# Load Environment Variables into System
load_dotenv(verbose=True)

# Slack keys from Env
slack_token = os.environ["SLACK_API_TOKEN"]

# Slack Python Web Client
slack_client = WebClient(token=slack_token)


def time_to_go(client, channel):
    client.chat_postMessage(
        channel=channel,
        text="I believe, every day, you should have at least one exquisite moment!! \n \n "
             "*10 minutes till the end of the work day!!* :heart:",
    )


def end_of_day(client, channel):
    client.chat_postMessage(
        channel=channel,
        text="I believe, every day, you should have at least one exquisite moment!! \n \n "
             "*It's the end of the work day!!* :heart:",
    )


def dy_end_of_day(client, channel):
    client.chat_postMessage(
        channel=channel,
        text="I believe, every day, you should have at least one exquisite moment! \n \n "
             "*It's the end of the work day!!* :heart: Dayeon!!! It's time to go home! :laughing:",
    )


def its10am(client, channel):
    client.chat_postMessage(
        channel=channel,
        text="I believe, every day, you should have at least one exquisite moment!! \n \n "
             "*It's 10 a.m.!!* :heart:",
    )


def message_dy(client):
    client.chat_postMessage(
        channel="ask_audrey",
        text="Hey Dayeon! It's Audrey! :blush: I heard that you won the table tennis match! Well done! :thumbsup: \n \n "
             "*Three cheers for Dayeon!* :raised_hands: *HOORAY* :raised_hands: *HOORAY* :raised_hands: *HOORAY!*",
    )


def mask_status(client):
    client.chat_postMessage(
        channel="ask_audrey",
        text="!mask 서울특별시 마포구 상암동"
    )


# Scheduled jobs
schedule.every().day.at("17:50").do(time_to_go, slack_client, "ask_audrey")
# schedule.every().day.at("18:00").do(end_of_day, slack_client, "ask_audrey")
schedule.every().day.at("10:00").do(its10am, slack_client, "ask_audrey")
schedule.every().day.at("16:50").do(dy_end_of_day, slack_client, "ask_audrey")
# schedule.every(5).minutes.do(mask_status, slack_client)

# schedule.every().day.at("13:05").do(message_dy, slack_client)

while True:
    schedule.run_pending()
    time.sleep(1)
