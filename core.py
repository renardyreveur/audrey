import commands
from utils import blossoms
from secret import db_user, db_pass
from db.sqlcontroller import SQLController

# SQL controller managing reading and updating data from the DB
controller = SQLController(host='localhost', port=3306, db='audrey_db', user=db_user, password=db_pass)

# All possible modules to search attribute(function)
# Make sure there isn't any naming collisions!
# modules = misc or hundred or food


# Core function dealing with incoming commands
def core(event_type, event_data, client):
    if event_type == "message":                             # If event is a message
        event = event_data['event']
        if "text" in event and len(event['text']) > 0\
                and event['text'][0] == "!":                # And starts with a '!'
            full_command = event['text'][1:]
            command = full_command.split(' ')[0]            # First word is command
            args = full_command.split(' ')[1:]              # The rest are arguments separated by space
            try:
                ret_val = getattr(commands, command)(*args)      # Run that function with the arguments
            except AttributeError:
                ret_val = None

            if ret_val is not None:                         # If good return, reply on Slack
                payload_type, payload = ret_val
                if payload_type == 'image':
                    client.chat_postMessage(
                        channel=event['channel'],
                        attachments=[{"title": command, "image_url": payload}],
                    )
                elif payload_type == 'text':
                    client.chat_postMessage(
                                channel=event['channel'],
                                text=payload,
                            )
                elif payload_type == 'file':
                    client.files_upload(
                        channels=event['channel'],
                        file=payload[0],
                        title=payload[1]
                    )

                elif payload_type == 'blocks':
                    client.chat_postMessage(
                        channel=event['channel'],
                        blocks=payload
                    )

    elif event_type == "reaction_added":                # If event is a reaction added
        event = event_data['event']
        blossoms(event, controller)

