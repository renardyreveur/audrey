FROM python:3.8-slim-buster

WORKDIR /audrey

COPY requirements.txt ./
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 80

ENV PATH="/audrey/webdriver:${PATH}"

CMD ["gunicorn", "-b", "0.0.0.0:80", "-w", "2", "main:app"]
