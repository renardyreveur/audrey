import os
import argparse
from slack import WebClient
from dotenv import load_dotenv

# Command line argument parser
parser = argparse.ArgumentParser(description="Send message as Audrey")

parser.add_argument('-c', '--channel', help='Channel to post message to')
parser.add_argument('-m', '--message', help='Message to send')

args = parser.parse_args()
if args.channel is not None:
    channel = args.channel
else:
    channel = 'ask_audrey'
    # channel = 'test'


if args.message is not None:
    message = args.message
else:
    message = "!mask 서울특별시 마포구 상암동"

# Load Environment Variables into System
load_dotenv(verbose=True)

# Slack keys from Env
slack_token = os.environ["SLACK_API_TOKEN_AIC"]

# Slack Python Web Client
slack_client = WebClient(token=slack_token)
print(args.message)
slack_client.chat_postMessage(
        channel=channel,
        text=message
    )