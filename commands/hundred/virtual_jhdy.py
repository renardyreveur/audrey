from collections import Counter
from itertools import groupby
import onnxruntime
import numpy as np

space_req_chars = '.,:ㅋㅎㅜㅠㄱㅇㄴ!?()'  # 을를는이가


def replace_space(corpus, chars):
    for c in chars:
        if c in corpus:
            corpus = corpus.replace(c, " "+c+" ")
    return corpus


def suffix_splitter(word):
    suffix = ["야", "은", "는", "이", "가", "를", "을", "의", "와", "에", "에게", "까지", "랑", "부터"]
    if word[-1] in suffix:
        word = word[:-1] + " " + word[-1]

    return word


def rep_breaker(s):
    i = (s+s).find(s, 1, -1)
    if i != -1 and len(s) > 2:
        reps = len(s)//len(s[:i])
        return ' '.join([s[:i]] * reps)
    else:
        return s


def handle_reps(word):
    # Group by repetitions
    reps = [''.join(g) for _, g in groupby(word)]

    # Separate the repetitions if repeated more than twice
    reps = [list(x) if len(x) > 2 else x for x in reps]

    # Index of the repetition list
    rep_idx = [i for i, x in enumerate(reps) if isinstance(x, list)]

    # List of the repetitions
    rep_list = [x for x in reps if isinstance(x, list)]

    # If there are repetitions in the word, add spaces in between
    if len(rep_idx) > 0:
        # Final position in expanding the given input word with repetitions separated with spaces
        word_pos = [x + 2 * len([item for sub_rep in rep_list[:i] for item in sub_rep]) for i, x in enumerate(rep_idx)]
        word_pos[-1] += 1

        for i in range(len(rep_list)):
            word = word[:word_pos[i]] + ' ' + ' '.join(reps[rep_idx[i]]) + ' ' + word[word_pos[i] + len(reps[rep_idx[i]]):]

    return word


def vocab_gen(text_file):
    # Read corpus text file
    with open(text_file, 'r') as tf:
        corpus = tf.readlines()

    corpus = ' '.join(corpus).strip()
    corpus = replace_space(corpus, space_req_chars)

    words = corpus.strip().split()

    # Characters e.g 응응응
    for i in range(len(words)):
        out = handle_reps(words[i])
        words[i] = out

    corpus = ' '.join(words)
    words = corpus.strip().split()

    # Words e.g 그래그래
    for i in range(len(words)):
        out = rep_breaker(words[i])
        words[i] = out

    corpus = ' '.join(words)

    words = corpus.strip().split()
    # Use collections.Counter to count all unique words in the corpus
    word_freq = Counter()

    word_freq.update(words)

    # Word - Idx - Frequency
    word2id = dict((word, i) for i, (word, _) in enumerate(iter(word_freq.items())))
    id2word = dict((i, word) for i, (word, _) in enumerate(iter(word_freq.items())))
    # word_idx_freq = dict((word, (i, freq)) for i, (word, freq) in enumerate(iter(word_freq.items())))
    text_in_idx = [word2id[word] for word in words]

    return word2id, id2word, text_in_idx


def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0)


def sample(preds, temperature=1.0):
    if not isinstance(preds, np.ndarray):
        preds = preds.detach().numpy()
    preds = softmax(preds)

    if temperature <= 0:
        return np.argmax(preds)
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    preds /= (sum(preds) + 0.00001)

    probas = np.random.multinomial(1, preds, 1)
    return int(np.argmax(probas))


# Vocabulary to tokenize input query and decode output sequence
word2id, id2word, _ = vocab_gen('commands/hundred/jdforest_corpus.txt')

ort_sess = onnxruntime.InferenceSession('commands/hundred/holidayeon.onnx')


def jeehoon(*prime):
    state_h, state_c = (np.zeros((1, 1, 128), dtype=np.float32), np.zeros((1, 1, 128), dtype=np.float32))

    prime = ['Dayeon', 'Shin', *prime]

    # Pre-processing
    prime = replace_space(' '.join(prime), space_req_chars)
    prime = prime.strip().split()

    for i in range(len(prime)):
        prime[i] = handle_reps(prime[i])
    prime = ' '.join(prime)
    prime = prime.strip().split()

    for i in range(len(prime)):
        prime[i] = rep_breaker(prime[i])
    prime = ' '.join(prime)
    prime = prime.strip().split()

    # for i in range(len(prime)):
    #     prime[i] = suffix_splitter(prime[i])
    # prime = ' '.join(prime)
    # prime = prime.strip().split()

    # Inference
    while True:
        result = []
        ort_out = None
        for w in prime:
            try:
                ix = np.array([[word2id[w]]])
            except KeyError:
                ix = np.array([[word2id['None']]])

            ort_inputs = {'input': ix, 'h0': state_h, 'c0': state_c}
            ort_out, state_h, state_c = ort_sess.run(['output', 'h', 'c'], ort_inputs)

        choice = sample(ort_out[0][0])
        result.append(id2word[choice])

        for _ in range(100):
            ix = np.array([[choice]])
            ort_inputs = {'input': ix, 'h0': state_h, 'c0': state_c}
            ort_out, state_h, state_c = ort_sess.run(['output', 'h', 'c'], ort_inputs)

            choice = sample(ort_out[0][0])
            result.append(id2word[choice])

        dy_idx = ' '.join(result).find('Dayeon Shin')
        jh_idx = ' '.join(result).find('Jeehoon Kang')

        if jh_idx == -1:
            jh_idx = 0

        if dy_idx == -1:
            dy_idx = 1000000000000000

        if dy_idx > jh_idx:
            response = ' '.join(result)[jh_idx+12:dy_idx]
            response.replace('Jeehoon Kang', '')
            break
    return 'text', "Jeehoon says: " + response


def dayeon(*prime):
    state_h, state_c = (np.zeros((1, 1, 128), dtype=np.float32), np.zeros((1, 1, 128), dtype=np.float32))

    prime = ['Jeehoon', 'Kang', *prime]

    # Pre-processing
    prime = replace_space(' '.join(prime), space_req_chars)
    prime = prime.strip().split()

    for i in range(len(prime)):
        prime[i] = handle_reps(prime[i])
    prime = ' '.join(prime)
    prime = prime.strip().split()

    for i in range(len(prime)):
        prime[i] = rep_breaker(prime[i])
    prime = ' '.join(prime)
    prime = prime.strip().split()

    # for i in range(len(prime)):
    #     prime[i] = suffix_splitter(prime[i])
    # prime = ' '.join(prime)
    # prime = prime.strip().split()

    # Inference
    while True:
        result = []
        ort_out = None
        for w in prime:
            try:
                ix = np.array([[word2id[w]]])
            except KeyError:
                ix = np.array([[word2id['None']]])

            ort_inputs = {'input': ix, 'h0': state_h, 'c0': state_c}
            ort_out, state_h, state_c = ort_sess.run(['output', 'h', 'c'], ort_inputs)

        choice = sample(ort_out[0][0])
        result.append(id2word[choice])

        for _ in range(100):
            ix = np.array([[choice]])
            ort_inputs = {'input': ix, 'h0': state_h, 'c0': state_c}
            ort_out, state_h, state_c = ort_sess.run(['output', 'h', 'c'], ort_inputs)

            choice = sample(ort_out[0][0])
            result.append(id2word[choice])

        dy_idx = ' '.join(result).find('Dayeon Shin')
        jh_idx = ' '.join(result).find('Jeehoon Kang')

        if jh_idx == -1:
            jh_idx = 1000000000

        if dy_idx == -1:
            dy_idx = 0

        if jh_idx > dy_idx:
            response = ' '.join(result)[dy_idx + 11:jh_idx]
            response.replace('Dayeon Shin', '')
            break
    return 'text', "Dayeon says: " + response
