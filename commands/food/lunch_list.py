from db.sqlcontroller import SQLController
from secret import db_host, db_user, db_pass

# SQL controller managing reading and updating data from the DB
controller = SQLController(host=db_host, port=3306, db='audrey_db', user=db_user, password=db_pass)


def ddu_list(*query):
    menu = controller.select_all('jeeddu_list')
    menu_dict = dict((x, y) for x, y in menu)
    response = []
    for i in range(len(menu)):
        menu_name = menu[i][1]
        del_key = menu[i][0]
        response.append(str(i + 1) + ". {} ({})".format(menu_name, del_key))

    if len(query) == 0:
        return 'text', '\n'.join(response)
    else:
        if query[0] not in ["add", "delete"]:
            return 'text', "Usage: !ddu_list / !ddu_list [add/delete] [menu name]"

        lunch = "('" + ' '.join(query[1:]) + "')"
        if query[0] == 'add':
            controller.insert('jeeddu_list', 'menu', lunch)
            return 'text', "Mmm!! Tasty!!! I like {} too! :smile:".format(' '.join(query[1:]))

        if query[0] == 'delete':
            controller.delete('jeeddu_list', 'food_id', lunch)
            return 'text', "Eww!! I don't like {} too! :stuck_out_tongue: ".format(menu_dict[int(query[1])])
