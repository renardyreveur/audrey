import random
import requests
import tempfile
import urllib.request
import logging
import os
logger = logging.getLogger(__name__)


# Choose a random element from a list of elements
def choose(*choices):
    if len(choices) > 0:
        choice = random.choice(choices)
        return 'text', choice
    else:
        return None


# At the end of the day, one has to die, who will it be?
def mafia(*choices):
    if len(choices) > 0:
        choices = list(choices)
        choices.pop(random.randrange(len(choices)))
        return 'text', ', '.join(choices)
    else:
        return None


def oracle(*question):
    if len(question) == 0:
        return 'text', "What wisdom do thy seek???"

    answers = [
        "ABSOLUTELEY :thumbsup:",
        "The Stars :Star: say NO",
        "So it shall be! :stars:",
        "YES :heart_eyes:",
        "No Doubt about it! :100:",
        "Indications say Yes :crossed_fingers:",
        "Prospect Good!!",
        "Unlikely :cry:",
        "Looks Like YES! :joy:",
        "Positively!",
        "Chances aren't good... :thumbsdown:",
        "Answer Unclear Ask Later!",
        "Focus and Ask Again :thinking_face:",
        "Don't Bet on it!",
        "Can't say now... ",
        "NO",
        "Very Likely!",
        "Cannot foretell now :sleepy:",
        "Consult me later :hourglass:",
        "You can count on it! :grin:"
        ]

    the_one = random.choice(answers)
    return 'text', the_one


def fox(*placeholder):
    resp = requests.get('https://randomfox.ca/floof/')
    fox_image = resp.json()['image']
    return 'image', fox_image


def puppy(*placeholder):
    choice = random.choice([0,1])

    if choice == 0:
        random_puppy = "https://random.dog/woof.json"
        resp = requests.get(random_puppy).json()
        temp = tempfile.NamedTemporaryFile(suffix=os.path.splitext(resp['url'])[1], delete=False)
        urllib.request.urlretrieve(resp['url'], temp.name)

        return 'file', (temp.name, "A cute pupper :dog: for you!!")

    else:
        resp = requests.get("https://dog.ceo/api/breeds/image/random")
        image_url = resp.json()['message']

        logger.info("Puppy: " + image_url)
        return 'image', image_url
