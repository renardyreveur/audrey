import wikiquote
import lyricsgenius
import random
import requests
from commands.misc.blocks import im_block_builder
from secret import genius_key, bing_key, merriam_dict_key, merriam_thes_key, korean_dict_key, korean_certkey_no
from xml.etree import ElementTree


# Quote of the Day! Command without user input
def qotd(*placeholder):
    q = wikiquote.qotd()
    return 'text', q[0] + "  - " + q[1]


def quote(*names):
    name = " ".join(names)
    try:
        q = random.choice(wikiquote.quotes(name))
    except wikiquote.utils.DisambiguationPageException:
        q = "Choose from: " + ", ".join(wikiquote.search(name))
    except wikiquote.utils.NoSuchPageException:
        q = "Couldn't find {}".format(name)
    except IndexError:
        q = "Please add a querying term!"

    return 'text', q


def lyrics(*query):
    query = " ".join(query)
    genius = lyricsgenius.Genius(genius_key)
    song = genius.search_song(query)
    return 'text', song.lyrics


def image(*query):
    subscription_key = bing_key
    search_url = "https://api.cognitive.microsoft.com/bing/v7.0/images/search"
    im_type = query[0]
    if im_type == "gif":
        im_type = "animatedgif"

    if im_type not in ["photo", "animatedgif"]:
        return "text", "The type should be either 'photo' or 'gif'!"

    search_term = " ".join(query[1:])

    headers = {"Ocp-Apim-Subscription-Key": subscription_key}
    params = {"q": search_term, "imageType": im_type, "safeSearch": "Off", "mkt": "ar-KW"}

    response = requests.get(search_url, headers=headers, params=params)
    response.raise_for_status()
    search_results = response.json()

    content_urls = [img["contentUrl"] for img in random.choices(search_results["value"], k=1)]
    im_block = im_block_builder(search_term, content_urls)
    return 'blocks', im_block


def define(*query):
    # Dictionary only takes one word
    # if len(query) != 1:
    #     return 'text', 'Please write down ONE word!'

    query = ' '.join(query)

    if query == 'Dayeon' or query == 'dayeon':
        defs = [
            "1. The *most beautiful woman* on earth :blossom: , loved endlessly, and passionately by Jeehoon :heart: :orange_heart: :yellow_heart: :green_heart: :blue_heart: :purple_heart:",
            "2. 'The _girl of my dreams_ :two_hearts: '- according to Jeehoon",
            "3. The most attractive, vigorously fragrant, tenderly soft, and unimaginably pretty flower :blossom: in the whole wide world, blooming just for Jeehoon, by Jeehoon, and with Jeehoon"
        ]
        return 'text', "*{}*, {} \n".format(query, "flower") + '\n'.join(defs)

    if query == 'Jeehoon' or query == 'jeehoon':
        defs = [
            "1. As the most pure and fabulous person in the world :cherry_blossom:, Dayeon falls in love anew every day. :sparkling_heart:",
            "2. Only DAYEON's. :kiss:",
            "3. Dayeon's delight :sparkles:, life :heart:, world :world_map:, earth :earth_americas:, moon :full_moon:, sun :sunflower:, countless stars :star2:, galaxies :night_with_stars:, and space :milky_way:",
            "4. 'I'm the happiest girl in the world when I'm with him :cherry_blossom:. This is _Belle Époque_ :sunrise:. Meeting him was the luckiest thing I've ever had in my life. :heart: :orange_heart: :yellow_heart: :green_heart: :blue_heart: :purple_heart:' - according to Dayeon :blossom: .",
        ]
        return 'text', "*{}*, {} \n".format(query, "Dayeon's husband") + '\n'.join(defs)

    # Merriam-Webster Dictionary API
    dict_key = merriam_dict_key
    api_url = 'https://www.dictionaryapi.com/api/v3/references/collegiate/json/{}?key={}'.format(query, dict_key)

    # Send request with query and key
    resp = requests.get(api_url)
    jsonresp = resp.json()

    # No match for word
    if len(jsonresp) == 0:
        return 'text', 'Please check your query!'

    # Possible spelling mistake
    if isinstance(jsonresp[0], str):
        return 'text', 'Did you mean one of : {}'.format(', '.join(jsonresp))

    # Correct Query
    pos = jsonresp[0]['fl']
    defn = jsonresp[0]['shortdef']

    # Create Definition
    defs = []
    for i in range(len(defn)):
        defs.append(str(i+1) + ". " + defn[i])

    return 'text', "*{}*, {} \n".format(query, pos) + '\n'.join(defs)


def syn(*query):
    # thesaurus only takes one word
    if len(query) != 1:
        return 'text', 'Please write down ONE word!'

    # Merriam-Webster API key
    thes_key = merriam_thes_key
    api_url = 'https://www.dictionaryapi.com/api/v3/references/thesaurus/json/{}?key={}'.format(query[0], thes_key)

    # Request with Query and Key
    resp = requests.get(api_url)
    jsonresp = resp.json()

    # No match for word
    if len(jsonresp) == 0:
        return 'text', 'Please check your query!'

    # Possible spelling mistake
    if isinstance(jsonresp[0], str):
        return 'text', 'Did you mean one of : {}'.format(', '.join(jsonresp))

    # Correct Query
    pos = jsonresp[0]['fl']
    syns = jsonresp[0]['meta']['syns'][0]

    return 'text', "*{}*, {} \n".format(query[0], pos) + ', '.join(syns)


def defkor(*query):
    # Dictionary only takes one word
    if len(query) != 1:
        return 'text', 'Please write down ONE word!'

    # 국어국립원 Dictionary API
    api_url = 'https://stdict.korean.go.kr/api/search.do?certkey_no={}&key={}&method={}&q={}'\
        .format(korean_certkey_no, korean_dict_key, 'wildcard', query[0])

    # Send request with query and key
    resp = requests.get(api_url)
    tree = ElementTree.fromstring(resp.content)

    defs = []
    i = 1

    for definition in tree.iter('definition'):
        defs.append(str(i) + ". " + definition.text)
        i += 1

    # No match for word
    if len(defs) == 0:
        return 'text', 'Please check your query!'

    return 'text', "*{}* \n".format(query[0]) + '\n'.join(defs)

