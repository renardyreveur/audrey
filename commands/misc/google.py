import requests
from secret import youtube_key


def youtube(*query):
    subscription_key = youtube_key
    search_url = "https://www.googleapis.com/youtube/v3/search"

    search_term = " ".join(query)

    params = {"part": "snippet", "maxResults": 5, "q": search_term, "key": subscription_key}

    response = requests.get(search_url, params=params)
    response.raise_for_status()
    search_results = response.json()

    if len(search_results["items"]) == 0:
        return 'text', "No videos found with that query!!"

    top_result = search_results["items"][0]["id"]["videoId"]
    vid_link = "https://www.youtube.com/watch?v={}".format(top_result)
    return 'text', vid_link
