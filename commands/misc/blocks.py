
def im_block_builder(text, url):
    im_block = [{
        "type": "image",
        "title": {
            "type": "plain_text",
            "text": text,
        },
        "image_url": url[0],
        "alt_text": "image1"
    }]

    return im_block
