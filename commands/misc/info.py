import time

from bs4 import BeautifulSoup
import requests
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
import datetime
from db.sqlcontroller import SQLController
from secret import db_host, db_user, db_pass

# SQL controller managing reading and updating data from the DB
controller = SQLController(host=db_host, port=3306, db='audrey_db', user=db_user, password=db_pass)


def get_html(url):
    _html = ""
    resp = requests.get(url)
    if resp.status_code == 200:
        _html = resp.text
    return _html


def exrate(*query):
    URL = "https://quotation-api-cdn.dunamu.com/v1/forex/recent?codes=FRX.KRW"
    if len(query) != 2:
        return None
    curr, price = query
    currencies = ['USD', 'GBP', "JPY", "CNY", "EUR", "RUB"]
    if curr not in currencies:
        return 'text', "Please choose between {}".format(", ".join(currencies))

    f_URL = URL + curr
    html = get_html(f_URL)
    soup = BeautifulSoup(html, 'html.parser')

    # country = str(soup).split('"country":"')[1].split('"')[0]
    # currencyN = str(soup).split('"currencyName":"')[1].split('"')[0]
    currency = str(soup).split('"cashBuyingPrice":')[1].split(',')[0]
    # print(country , '|',currencyN,'1',curr,": \ ", currency)

    input_price = price
    adjusted_price = float(input_price) * float(currency)
    return 'text', "₩ " + f"{round(adjusted_price):,d}"


def astro(*sign):
    if len(sign) != 1:
        return 'text', "Please tell me your star sign only!"
    if sign[0] not in ["aries", "taurus", "gemini", "cancer", "leo", "virgo", "libra",
                       "scorpio", "sagittarius", "capricorn", "aquarius", "pisces"]:
        return 'text', "Please choose one of the 12 signs! : aries, taurus, gemini, cancer, leo, virgo, libra, \
                       scorpio, sagittarius, capricorn, aquarius, pisces"

    params = (
        ('sign', sign[0]),
        ('day', 'today'),
    )

    resp = requests.post('https://aztro.sameerkumar.website/', params=params)
    horoscope = resp.json()['description']
    # date_range, current date, compatibility, mood, color, lucky_number, lucky_time

    return 'text', horoscope


def fortune(*query):
    if len(query) != 2:
        return 'text', 'Please write down your gender and birthday! (!fortune [male/female] YYYYMMDD)'

    # Web driver for Firefox to dynamically query web
    opts = Options()
    opts.headless = True
    browser = Firefox(options=opts, service_log_path="/dev/null")
    browser.get("https://search.naver.com/search.naver?sm=top_hty&fbm=1&ie=utf8&query=%EC%9A%B4%EC%84%B8'")
    time.sleep(1)

    gender, birthday = query
    if gender not in ["male", "female"]:
        return 'text', "Gender is either male or female"
    try:
        datetime.datetime.strptime(birthday, '%Y%m%d')
    except ValueError:
        return 'text', "Check your birthday, write it in YYYYMMDD format"
    if gender == "male":
        browser.find_element_by_xpath(
            'html/body/div[3]/div[2]/div/div[1]/div[1]/div/div[2]/div/div[2]/div[1]/fieldset/div[1]/span[1]/a').click()

    else:
        browser.find_element_by_xpath(
            '/html/body/div[3]/div[2]/div/div[1]/div[1]/div/div[2]/div/div[2]/div[1]/fieldset/div[1]/span[2]/a').click()
    browser.find_element_by_xpath('//*[@id="srch_txt"]').click()
    bday = browser.find_element_by_id('srch_txt')
    bday.send_keys(birthday)

    browser.find_element_by_xpath(
        '/html/body/div[3]/div[2]/div/div[1]/div[1]/div/div[2]/div/div[2]/div[1]/fieldset/input').click()

    gen_result = browser.find_element_by_xpath(
        '/html/body/div[3]/div[2]/div/div[1]/div[1]/div/div[2]/div/div[2]/div[2]/dl[1]/dd/strong').text
    full_result = browser.find_element_by_xpath(
        '/html/body/div[3]/div[2]/div/div[1]/div[1]/div/div[2]/div/div[2]/div[2]/dl[1]/dd/p').text
    browser.close()

    return 'text', "*" + gen_result + "*" + "\n" + full_result


def gift(*query):
    bls = controller.select_all('citizens')
    jh_bls = bls[0][2]
    dy_bls = bls[1][2]

    if len(query) == 0:
        return 'text', "You guys have been very kind to each other! :blush: \n" \
                       " :cherry_blossom:Jeehoon:cute_fox: :   *{}*              :blossom:Dayeon:run_penguin: :   *{}*"\
            .format(jh_bls, dy_bls)
    else:
        try:
            amount = int(query[2])
        except:
            return 'text', "Usage: !gift [deduct] [name] [amount]"
        if query[0] == 'deduct':
            if query[1] == 'dayeon' or query[1] == 'Dayeon':  # 다연
                controller.update('citizens', 'blossoms', dy_bls - amount, 'user_id', bls[1][0])
                return 'text', ":sob: Dayeon!! Noo....."

            if query[1] == 'jeehoon' or query[1] == 'Jeehoon':  # 지훈
                controller.update('citizens', 'blossoms', jh_bls - amount, 'user_id', bls[0][0])
                return 'text', ":sob: Jeehoon!! Noo..."
        else:
            return 'text', "Usage: !gift [deduct] [name] [amount]"


def mask(*query):
    mask_url = "https://8oi9s0nnth.apigw.ntruss.com/corona19-masks/v1/storesByAddr/json?address=" + ' '.join(query)
    resp = requests.get(mask_url)
    if resp.status_code == 200:
        json = resp.json()
    else:
        return 'text', 'Oops! Something went wrong! Talk to Jeehoon > <'

    sort_key = {'plenty': 0, 'some': 1, 'few': 2, 'empty': 3, None: 4, 'break': 5}
    stores = json['stores']
    if len(stores) == 0:
        return 'text', "Couldn't find results for your query :tears:"
    max_show = 10
    stores = stores[:max_show]
    stores = sorted(stores, key=lambda k: sort_key[k['remain_stat']])

    response = []
    for i in range(len(stores)):
        address = stores[i]['addr']
        name = stores[i]['name']
        store_type = stores[i]['type']
        type_dict = {"01": "약국", "02": "우체국", "03": "농협", None: "?"}
        stock_at = stores[i]['stock_at']
        remain_stat = stores[i]['remain_stat']
        remain_dict = {'plenty': "100개 이상 :heavy_check_mark: ", 'some': "30-100개 :white_check_mark: ",
                       'few': "2-30개  :heavy_minus_sign: ", 'empty': "1개 이하 :x: ", None: "?", "break": "판매중지 :end:"}

        if stock_at is None:
            stock_at = "?"

        response.append(str(i + 1) + ". {}, {}, 종류: {}. \n 입고 시간: {}, \n *마스크 수량: {}* \n"
                        .format(address, name, type_dict[store_type], stock_at, remain_dict[remain_stat]))

    return 'text', "*BE SAFE!* :mask: :heart: \n \n" + '\n'.join(response)


def dday(*query):
    ddays = list(controller.select_all('dday'))
    today = datetime.date.today()

    events = [x[1] for x in ddays]

    if len(query) == 0:
        ddays = sorted([a[1:] for a in ddays], key=lambda x: x[1])

        all_ddays = []
        i = 1
        for event in ddays:
            days_left = event[1] - today
            all_ddays.append(str(i) + ". " + event[0] + "     " + "Date: {}".format(event[2]) + "    Days Left: {}".format(days_left.days))
            i += 1
        return 'text', '\n'.join(all_ddays)

    elif len(query) == 1:
        event_idx = events.index(query[0])
        event = ddays[event_idx]
        days_left = event[2] - today
        return 'text', "You have {} days remaining until {}!!".format(days_left.days, event[1])

    elif len(query) == 3:
        if query[0] != "register":
            return 'text', "Usage: !dday [register] [event] [date YYYY-MM-DD]"

        event_name = query[1]
        dueday = query[2]
        try:
            datetime.datetime.strptime(dueday, '%Y-%m-%d')
        except ValueError:
            return 'text', "Check the date, write it in YYYY-MM-DD format!"

        days_left = datetime.datetime.strptime(dueday, '%Y-%m-%d').date() - today

        if days_left.days < 0:
            return 'text', "Check the date, Please choose a future date!"

        controller.insert('dday', '(event, date)', (event_name, dueday))

        return 'text', "Registered! There are {} days left till {}! :thumbsup:".format(days_left.days, event_name)

    else:
        return 'text', "Usage: !dday [register] [event] [date YYYY-MM-DD] / !dday [event] / !dday"


def help(*placeholder):
    with open('all_func.txt', 'r') as f:
        functions = f.readlines()
    functions = [x.rstrip() for x in functions]
    functions.sort()
    return 'text', ', '.join(functions)
