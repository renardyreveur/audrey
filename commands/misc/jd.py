import datetime
import calendar
import random
from db.sqlcontroller import SQLController
from secret import db_host, db_user, db_pass


# SQL controller managing reading and updating data from the DB
controller = SQLController(host=db_host, port=3306, db='audrey_db', user=db_user, password=db_pass)

# I really like this function
# Likewise here! Dayeon!


def jhdy(*query):
    list_emo = [':laughing:', ':blush:', ':wink:', ":four_leaf_clover:", ":rainbow:", ":unicorn_face:",
                ":sunflower:", ":strawberry:", ":hibiscus:"]

    if len(query) == 0:
        return 'text', "Usage: {등록} !jhdy 등록 YYYY-MM-DD 놀러갈 곳 / {조회} !jhdy [암구호]"

    if query[0] == "등록":
        date = query[1]
        loc = ' '.join(query[2:])
        try:
            datetime.datetime.strptime(date, '%Y-%m-%d')
        except ValueError:
            return 'text', "Check the date, write it in YYYY-MM-DD format!"

        if len(loc) > 10000:
            return 'text', "The Location parameter is too long!"
        try:
            controller.insert('jhdy', '(date, location)', (date, loc))
            return 'text', "Yay!! another adventure for you two:blossom::cherry_blossom:!!! :blush: \n" \
                           "(I wanna go too... :cry: - Audrey)"
        except:
            return 'text', "Oops, Something went wrong, talk to Jeehoon >.<"

    elif query[0] == "삭제":
        controller.delete()
    elif query[0] == "show":
        try:
            adventures = controller.select_all('jhdy')
        except:
            return 'text', "Oops, Something went wrong, talk to Jeehoon >.<"

        if len(adventures) == 0:
            return 'text', "You guys don't have any adventures planned yet! :sob:"
        adventures = sorted([a[1:] for a in adventures], key=lambda x: x[0])
        print(adventures)
        ads = []
        for i in range(len(adventures)):
            ads.append(str(i + 1) + ".  {}({}),  {} {}"
                       .format(adventures[i][0], calendar.day_name[adventures[i][0].weekday()][:3], adventures[i][1], random.choice(list_emo)))

        return 'text', "*Adventures of Dayeon* :blossom: *and Jeehoon* :cherry_blossom:*!!!* \n" + '\n'.join(ads)

    else:
        return 'text', 'Are you sure you know how to use this?! INTRUDER ALERT!'


def applepie(*query):
    the_start_of_everything = datetime.date(2020, 3, 14)
    today = datetime.date.today()
    days_full_of_love = today - the_start_of_everything
    return 'text', "You guys have been exploring the world, and loving each other _whole-heartedly_ for *{} days*! :heart:".format(days_full_of_love.days)
